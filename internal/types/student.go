package types

import (
	"fmt"
)

type Student struct {
	Index    uint16
	Snils    string
	SpecCode string
	SpecName string
	Budget   uint8
	Bonus    uint8
	Score    uint16
}

type Students []Student

func (s *Student) String() string {
	var budg bool
	switch s.Budget {
	case 1:
		budg = true
	case 0:
		budg = false
	default:
		budg = true
	}

	return fmt.Sprintf("Место в списке: %d\n"+
		"Номер СНИЛСа или договора: %s\n"+
		"Полное название специальности: %s %s\n"+
		"Бюджет: %t\n"+
		"Бонусные баллы: %d\n"+
		"Всего баллов: %d\n", s.Index, s.Snils, s.SpecCode, s.SpecName, budg, s.Bonus,
		s.Score)
}

func NewStudent() *Student {
	return &Student{}
}
