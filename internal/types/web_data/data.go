package web_data

import "busterd/scrap_pgu/internal/types"

type Web struct {
	AppName  string
	Students types.Students
	Render   bool
}
