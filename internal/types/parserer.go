package types

import (
	"busterd/scrap_pgu/internal/types/logger"
	"regexp"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/gocolly/colly"
	"github.com/rs/zerolog"
)

type Parser struct {
	Url    string
	logger zerolog.Logger
}

func (p *Parser) createLogger() {
	p.logger = logger.New(0)
}

func (p *Parser) Run() Students {
	p.createLogger()

	var students Students
	c := colly.NewCollector(
		colly.AllowedDomains("pnzgu.ru"),
	)

	url_s := strings.Split(p.Url, "/")
	if len(url_s) < 4 {
		p.logger.Error().Msgf("Введён неправильный или пустой URL: %s", p.Url)
		return nil
	}
	if url_s[2] != "pnzgu.ru" && url_s[3] != "apply" && url_s[4] != "list" {
		p.logger.Error().Msgf("Введён неправильный URL: %s", p.Url)
		return nil
	}

	c.OnHTML(".content-page", func(h *colly.HTMLElement) {
		var spec string
		var spec_c string
		var spec_n string
		specs := h.DOM.Find(`.list_select[name="speciality"]`).Children()

		specs.Each(func(i int, s *goquery.Selection) {
			_, ok := s.Attr("selected")
			if ok {
				spec = s.Text()
				re_code := `(\d+\.)+\d+`
				re_spec := `\d+\.\d+\.\d+\s+(.*)`
				re_c := regexp.MustCompile(re_code)
				re_s := regexp.MustCompile(re_spec)

				spec_c = re_c.FindString(spec)
				match_s := re_s.FindStringSubmatch(spec)
				if len(match_s) > 1 {
					spec_n = match_s[1]
				}
			}
		})

		h.ForEach("tr", func(i int, h *colly.HTMLElement) {
			s := NewStudent()
			h.ForEach("td", func(i int, h *colly.HTMLElement) {
				switch i {
				case 1:
					s.Snils = h.Text
				case 3:
					bud := h.Text
					switch bud {
					case "б":
						s.Budget = 1
					case "д":
						s.Budget = 0
					default:
						s.Budget = 1
					}
				case 5:
					bonus, err := strconv.Atoi(h.Text)
					if err != nil {
						p.logger.Fatal().Err(err).Msg("Невозможно определить количество бонусных баллов")
					}
					s.Bonus = uint8(bonus)
				case 6:
					score, err := strconv.Atoi(h.Text)
					if err != nil {
						p.logger.Fatal().Err(err).Msg("Невозможно определить количество баллов")
					}
					s.Score = uint16(score)
				}
			})
			s.SpecCode = spec_c
			s.SpecName = spec_n
			students = append(students, *s)
		})

		pages := h.DOM.Add(".pages_container")
		childs := pages.Children().Nodes[len(pages.Children().Nodes)-1]
		if len(childs.Attr) > 0 {
			if childs.Attr[0].Key == "href" {
				link := childs.Attr[0].Val
				c.Visit(h.Request.AbsoluteURL(link))
			}
		}
	})

	c.OnRequest(func(r *colly.Request) {
		p.logger.Debug().Msgf("Посещаю %s", r.URL)
	})

	c.OnError(func(r *colly.Response, err error) {
		p.logger.Error().Err(err).Send()
	})

	c.Visit(p.Url)
	return students
}
