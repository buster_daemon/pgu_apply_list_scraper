package app

import (
	"github.com/urfave/cli/v2"
)

type Application struct {
	App *cli.App
}

func NewApp() *Application {
	a := Application{
		App: &cli.App{
			Name:    "ПГУСИ",
			Usage:   "ПГУ Скрапер Информации",
			Version: "0.2.0",
			Description: "ПГУСИ предназначен для поиска информации об абитуриентах" +
				" подавших заявление на обучение в ПГУ",
			Commands: []*cli.Command{},
		},
	}
	a.addCommands()

	return &a
}
