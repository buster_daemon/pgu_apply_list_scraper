package app

import (
	"github.com/urfave/cli/v2"
)

type Commander interface {
	addCommands()
}

func (ac *Application) addCommands() {
	commands := []*cli.Command{
		{
			Name:    "scrap",
			Aliases: []string{"sr"},
			Usage:   "Собирает информацию с сайта",
			Flags: []cli.Flag{
				&cli.IntFlag{
					Name:  "dlevel",
					Usage: "Уровень вывода техических сообщений (-1 - 7)",
					Value: 1,
				},
			},
			Action: defaultScrap,
		},
		{
			Name:  "add",
			Usage: "Добавить ссылку/ссылки в базу для обработки",
			Flags: []cli.Flag{
				&cli.StringSliceFlag{
					Name:     "url",
					Usage:    "Ссылки для добавления (должны быть разделены запятыми)",
					Required: true,
				},
				&cli.IntFlag{
					Name:  "dlevel",
					Usage: "Уровень вывода техических сообщений (-1 - 7)",
					Value: 1,
				},
			},
			Action: addUrls,
		},
		{
			Name:    "show",
			Aliases: []string{"s"},
			Usage:   "Отображает информацию об абитуриентах интересующей группы",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:  "path",
					Value: "students.db",
					Usage: "Путь до базы данных со студентами",
				},
				&cli.StringFlag{
					Name:  "type",
					Value: "",
					Usage: "Выбрать тип отображения студентов (бюджет/договор)\n" +
						"Возможные варианты: пусто/budg/contract",
				},
				&cli.UintFlag{
					Name:  "limit",
					Value: 30,
					Usage: "Размер выборки студентов (чел.)",
				},
				&cli.UintFlag{
					Name:  "offset",
					Value: 0,
					Usage: "С какого места начинать выборку",
				},
				&cli.StringFlag{
					Name:        "snils",
					Usage:       "Номер СНИЛС для поиска",
					DefaultText: "XXX-XXX-XXX XX",
					Required:    true,
				},
				&cli.IntFlag{
					Name:  "dlevel",
					Usage: "Уровень вывода техических сообщений (-1 - 7)",
					Value: 1,
				},
			},
			Action: searchSnils,
		},
		{
			Name:  "web",
			Usage: "Запускает веб-интерфейс",
			Flags: []cli.Flag{
				&cli.IntFlag{
					Name:  "port",
					Usage: "Порт веб-интерфейса",
					Value: 8080,
				},
				&cli.IntFlag{
					Name:  "dlevel",
					Usage: "Уровень вывода техических сообщений (-1 - 7)",
					Value: 1,
				},
			},
			Action: startWebServer,
		},
	}

	ac.App.Commands = append(ac.App.Commands, commands...)
}
