package app

import (
	"busterd/scrap_pgu/internal/types"
	"busterd/scrap_pgu/internal/types/logger"
	"busterd/scrap_pgu/internal/types/web_data"
	"embed"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"strings"

	"github.com/pkg/browser"
	"github.com/urfave/cli/v2"
)

//go:embed html/*
var htmls embed.FS

func defaultScrap(ctx *cli.Context) error {
	var log = logger.New(ctx.Int("dlevel"))
	var studs types.Students
	d := types.NewDatabase()
	d.DBConnect()
	defer d.Connection.Close()

	urls, err := d.GetUrls()
	if err != nil {
		log.Error().Err(err).Send()
		return err
	}

	for _, url := range urls {
		p := types.Parser{
			Url: *url,
		}
		studs = append(studs, p.Run()...)
	}

	for _, stud := range studs {
		err = d.InsertStudent(&stud)
		if err != nil {
			log.Warn().Err(err).Send()
			continue
		}
	}

	return nil
}

func addUrls(ctx *cli.Context) error {
	var log = logger.New(ctx.Int("dlevel"))
	db := types.NewDatabase()
	db.DBConnect()
	defer db.Connection.Close()

	urls := ctx.StringSlice("url")
	for _, url := range urls {
		err := db.InsertUrls(url)
		if err != nil {
			log.Error().Err(err).Send()
			continue
		}
	}
	return nil
}

func searchSnils(ctx *cli.Context) error {
	var log = logger.New(ctx.Int("dlevel"))
	d := types.NewDatabase()
	d.Path = ctx.String("path")
	d.DBConnect()
	defer d.Connection.Close()

	stud, err := d.GetStudent(ctx.String("snils"), ctx.String("type"))
	if err != nil {
		log.Error().Err(err).Send()
		return err
	}

	for _, s := range *stud {
		fmt.Println(s.String())
	}

	return nil
}

func startWebServer(ctx *cli.Context) error {
	var log = logger.New(ctx.Int("dlevel"))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		var log = logger.New(ctx.Int("dlevel"))
		tmpl := template.Must(
			template.New("index.html").ParseFS(htmls, "html/index.html"),
		)

		var spec string = r.PostFormValue("spec")
		var snils string = r.PostFormValue("snils")
		lim, err := strconv.Atoi(r.PostFormValue("lim"))
		if err != nil {
			log.Debug().Err(err).Msg("Неизвестно количество абитуриентов для отображения")
			lim = 30
		}

		offset, err := strconv.Atoi(r.PostFormValue("offset"))
		if err != nil {
			log.Debug().Err(err).Msg("Неизвестно значение начала отсчёта мест")
			offset = 0
		}

		data := web_data.Web{
			AppName: ctx.App.Name,
			Render:  false,
		}

		if snils == "" {
			if spec != "" {
				db := types.NewDatabase()
				db.DBConnect()
				defer db.Connection.Close()

				data.Students, _ = db.GetStudents(spec, r.PostFormValue("display"),
					uint8(lim), uint8(offset))
				data.Render = true
			}
		}

		if snils != "" {
			db := types.NewDatabase()
			db.DBConnect()
			defer db.Connection.Close()

			s, err := db.GetStudent(snils, r.PostFormValue("display"))
			if err != nil {
				log.Error().Err(err).Send()
				return
			}

			data.Students = *s
			data.Render = true
		}

		err = tmpl.ExecuteTemplate(w, "index.html", data)
		if err != nil {
			log.Error().Err(err).Send()
		}
	})

	http.HandleFunc("/add", func(w http.ResponseWriter, r *http.Request) {
		var log = logger.New(ctx.Int("dlevel"))
		var Urls []string
		d := types.NewDatabase()
		d.DBConnect()

		specsUrls, err := d.GetUrls()
		if err != nil {
			log.Error().Err(err).Send()
		}
		for _, url := range specsUrls {
			Urls = append(Urls, *url)
		}

		tmpl, err := template.New("addSpec.html").ParseFS(htmls, "html/addSpec.html")
		if err != nil {
			log.Error().Err(err).Send()
		}

		var specs string = r.PostFormValue("specs")
		sli_urls := strings.Split(specs, ",")
		if len(sli_urls) > 0 {
			go func() {
				db := types.NewDatabase()
				db.DBConnect()
				defer db.Connection.Close()

				for _, url := range sli_urls {
					db.InsertUrls(url)
				}
			}()
		}

		if r.PostFormValue("scan") != "" {
			go func() {
				var studs types.Students
				d := types.NewDatabase()
				d.DBConnect()
				defer d.Connection.Close()

				urls, err := d.GetUrls()
				if err != nil {
					log.Error().Err(err).Send()
					return
				}

				for _, url := range urls {
					p := types.Parser{
						Url: *url,
					}
					studs = append(studs, p.Run()...)
				}

				for _, stud := range studs {
					err = d.InsertStudent(&stud)
					if err != nil {
						log.Warn().Err(err).Send()
						continue
					}
				}
			}()
		}

		tmpl.Execute(w, Urls)
	})

	err := browser.OpenURL("http://localhost:" + fmt.Sprintf("%d", ctx.Int("port")))
	if err != nil {
		log.Error().Err(err).Send()
	}

	err = http.ListenAndServe(":"+fmt.Sprintf("%d", ctx.Int("port")), nil)
	if err != nil {
		log.Error().Err(err).Send()
		return err
	}
	return nil
}
