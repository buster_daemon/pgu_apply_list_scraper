package logger

import (
	"os"
	"time"

	"github.com/rs/zerolog"
)

func New(level int) zerolog.Logger {
	var log_level zerolog.Level

	consoleWriter := zerolog.ConsoleWriter{
		Out:        os.Stdout,
		TimeFormat: time.RFC1123,
	}
	fileWriter, _ := os.OpenFile(
		"logs.log",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY,
		0664,
	)
	multi := zerolog.MultiLevelWriter(consoleWriter, fileWriter)

	switch level {
	case -1:
		log_level = zerolog.TraceLevel
	case 0:
		log_level = zerolog.DebugLevel
	case 1:
		log_level = zerolog.InfoLevel
	case 2:
		log_level = zerolog.WarnLevel
	case 3:
		log_level = zerolog.ErrorLevel
	case 4:
		log_level = zerolog.FatalLevel
	case 5:
		log_level = zerolog.PanicLevel
	case 6:
		log_level = zerolog.NoLevel
	case 7:
		log_level = zerolog.Disabled
	default:
		log_level = zerolog.DebugLevel
	}

	zerolog.SetGlobalLevel(log_level)

	return zerolog.New(multi).With().Timestamp().Caller().Logger()
}
