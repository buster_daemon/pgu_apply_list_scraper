package types

import (
	"busterd/scrap_pgu/internal/types/logger"
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
	"github.com/rs/zerolog"
)

type Database struct {
	Path       string
	log        zerolog.Logger
	Connection *sql.DB
}

type urlError struct {
	message string
}

func (u *urlError) Error() string {
	return u.message
}

func NewDatabase() *Database {
	return &Database{log: logger.New(1)}
}

func (d *Database) DBConnect() error {
	if d.Path == "" {
		d.log.Debug().Msg("Отсутствует путь до БД, использую дефолтную")
		d.Path = "students.db"
	}

	db, err := sql.Open("sqlite3", d.Path)
	if err != nil {
		d.log.Error().Err(err).Send()
		return err
	}

	d.Connection = db
	err = d.initTable()
	if err != nil {
		d.log.Error().Err(err).Send()
		return err
	}

	return nil
}

func (d *Database) initTable() error {
	_, err := d.Connection.Exec(`
	CREATE TABLE IF NOT EXISTS students (
		snils TEXT,
		codeSpec TEXT,
		nameSpec TEXT,
		budget INTEGER,
		bonus INTEGER,
		score INTEGER
	);
	CREATE UNIQUE INDEX IF NOT EXISTS students_snils_IDX ON students (snils,codespec);
	CREATE TABLE IF NOT EXISTS links (
		url TEXT,
		CONSTRAINT links_PK PRIMARY KEY (url)
	);
	`)
	if err != nil {
		d.log.Error().Err(err).Send()
		return err
	}

	_, err = d.Connection.Exec(`
	DELETE
	FROM
		LINKS
	WHERE
		URL IS NULL
		OR TRIM(URL) = ""
		OR ""
	`)
	if err != nil {
		d.log.Error().Err(err).Send()
		return err
	}

	return nil
}

func (d *Database) InsertUrls(url string) error {
	if url == "" {
		d.log.Error().Err(&urlError{message: "Пустая ссылка"}).Send()
		return &urlError{message: "Пустая ссылка"}
	}
	_, err := d.Connection.Exec(`
	INSERT OR REPLACE INTO links VALUES (?)
	`, url)
	if err != nil {
		d.log.Error().Err(err).Send()
		return err
	}
	return nil
}

func (d *Database) GetUrls() ([]*string, error) {
	rows, err := d.Connection.Query(`
	SELECT url FROM links
	`)
	if err != nil {
		d.log.Error().Err(err).Send()
		return nil, err
	}

	var urls []*string
	for rows.Next() {
		var url string
		err = rows.Scan(&url)
		if err != nil {
			d.log.Warn().Err(err).Send()
			continue
		}

		urls = append(urls, &url)
	}

	return urls, nil
}

func (d *Database) InsertStudent(s *Student) error {
	_, err := d.Connection.Exec(`
	INSERT OR REPLACE INTO students VALUES (?, ?, ?, ?, ?, ?)
	`, s.Snils, s.SpecCode, s.SpecName, s.Budget, s.Bonus, s.Score)
	if err != nil {
		d.log.Error().Err(err).Send()
		return err
	}
	return nil
}

func (d *Database) GetStudents(codeSpec string, display string, limit uint8,
	offset uint8) (Students, error) {
	var query string

	switch display {
	case "budg":
		query = `
		SELECT snils, codeSpec, nameSpec, budget, bonus, score FROM students
		WHERE
		codeSpec = ?
		AND
		score > 10
		AND
		budget = 1
		ORDER BY score DESC
		LIMIT ? OFFSET ?
		`
	case "contract":
		query = `
		SELECT snils, codeSpec, nameSpec, budget, bonus, score FROM students
		WHERE
		codeSpec = ?
		AND
		score > 10
		AND
		budget = 0
		ORDER BY score DESC
		LIMIT ? OFFSET ?`
	default:
		query = `
		SELECT snils, codeSpec, nameSpec, budget, bonus, score FROM students
		WHERE
		codeSpec = ?
		AND
		score > 10
		ORDER BY score DESC
		LIMIT ? OFFSET ?
	`
	}
	statement, err := d.Connection.Prepare(query)
	if err != nil {
		d.log.Error().Err(err).Send()
		return nil, err
	}

	studs_rows, err := statement.Query(codeSpec, limit, offset)
	if err != nil {
		d.log.Error().Err(err).Send()
		return nil, err
	}
	var studs Students
	var i uint16 = 1

	for studs_rows.Next() {
		var s Student
		err = studs_rows.Scan(&s.Snils, &s.SpecCode, &s.SpecName, &s.Budget,
			&s.Bonus, &s.Score)
		if err != nil {
			d.log.Warn().Err(err).Send()
			continue
		}
		s.Index = i
		studs = append(studs, s)
		i++
	}
	defer studs_rows.Close()

	return studs, nil
}

func (d *Database) GetStudent(snils string, display string) (*Students, error) {
	var query string

	switch display {
	case "budg":
		query = `
		SELECT
			*
		FROM
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CODESPEC) I,
				*
			FROM
				(
				SELECT
					S.SNILS,
					S.CODESPEC,
					S.NAMESPEC,
					S.BUDGET,
					S.BONUS,
					S.SCORE
				FROM
					STUDENTS S
				WHERE
					SCORE > 10
					AND BUDGET = 1
				ORDER BY
					SCORE DESC
				) )
		WHERE
			SNILS = ?
		ORDER BY
			I ASC
		`
	case "contract":
		query = `
		SELECT
			*
		FROM
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CODESPEC) I,
				*
			FROM
				(
				SELECT
					S.SNILS,
					S.CODESPEC,
					S.NAMESPEC,
					S.BUDGET,
					S.BONUS,
					S.SCORE
				FROM
					STUDENTS S
				WHERE
					SCORE > 10
					AND BUDGET = 0
				ORDER BY
					SCORE DESC
				) )
		WHERE
			SNILS = ?
		ORDER BY
			I ASC
		`
	default:
		query = `
		SELECT
			*
		FROM
			(
			SELECT
				ROW_NUMBER() OVER (PARTITION BY CODESPEC) I,
				*
			FROM
				(
				SELECT
					S.SNILS,
					S.CODESPEC,
					S.NAMESPEC,
					S.BUDGET,
					S.BONUS,
					S.SCORE
				FROM
					STUDENTS S
				ORDER BY
					SCORE DESC
				) )
		WHERE
			SNILS = ?
		ORDER BY
			I ASC
		`
	}
	statement, err := d.Connection.Prepare(query)
	if err != nil {
		d.log.Error().Err(err).Send()
		return nil, err
	}

	rows, err := statement.Query(snils)
	if err != nil {
		d.log.Error().Err(err).Send()
		return nil, err
	}

	var ss Students
	for rows.Next() {
		var s Student
		err = rows.Scan(&s.Index, &s.Snils, &s.SpecCode, &s.SpecName, &s.Budget,
			&s.Bonus, &s.Score)
		if err != nil {
			d.log.Warn().Err(err).Send()
			continue
		}
		ss = append(ss, s)
	}

	return &ss, nil
}
