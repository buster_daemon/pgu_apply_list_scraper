package main

import (
	"busterd/scrap_pgu/internal/types/app"
	"busterd/scrap_pgu/internal/types/logger"
	"os"
)

func main() {
	log := logger.New(1)

	applic := app.NewApp()
	err := applic.App.Run(os.Args)
	if err != nil {
		log.Fatal().Err(err).Send()
	}
}
